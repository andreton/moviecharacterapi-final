﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharacterAPI.Model
{
    public class MovieCharacter
    {
        public string PictureUrl { get; set; }

        /*Actor*/
        public int ActorId { get; set; }
        public Actor Actor { get; set; }

        /*Character*/
        public int CharacterId { get; set; }
        public Character Character { get; set; }

        /*Movie*/
        public int MovieId { get; set; }
        public  Movie Movie{ get; set; }
    }
}

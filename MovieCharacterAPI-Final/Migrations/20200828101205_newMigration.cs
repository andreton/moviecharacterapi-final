﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace MovieCharacterAPI_Final.Migrations
{
    public partial class newMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(table: "Franchises",
     columns: new[] { "Name", "Description" },
     values: new object[,] { { "The lord of the rings ", "An epic fantasy saga, based on the books by JRR Tolkien" },
                                        { "Batman ", "The story about the masked vigilante" }});

            migrationBuilder.InsertData(
              table: "Movie",
              columns: new[] { "MovieTitle", "Genres", "ReleaseYear", "Director", "PictureUrl", "Trailer", "FranchiseId" },
              values: new object[,] { { "The fellowship of the ring", "Adventure", new DateTime(2001, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Peter Jackson", "https://www.imdb.com/title/tt0120737/mediaviewer/rm3592958976?ref_=tt_ov_i", "https://www.imdb.com/title/tt0120737/mediaviewer/rm3592958976?ref_=tt_ov_i", 1 },
                                        {"Batman and Robin", "Action", new DateTime(1988, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),"Joel Schumacker","https://en.wikipedia.org/wiki/Batman_%26_Robin_(film)#/media/File:Batman_&_Robin_poster.jpg", "https://www.youtube.com/watch?v=4RBXypX4qWI", 2},
                                        {"The dark knight","Action",new DateTime(2008, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),"Christopher Nolan","https://en.wikipedia.org/wiki/The_Dark_Knight_(film)#/media/File:Dark_Knight.jpg", "https://www.imdb.com/title/tt0120737/mediaviewer/rm3592958976?ref_=tt_ov_i", 2} }
       );

            migrationBuilder.InsertData(table: "Characters",
              columns: new[] { "FullName", "Alias", "Gender", "PictureUrl" },
                values: new object[,]{ {"Batman","The dark knight", "Male","https://no.wikipedia.org/wiki/Batman#/media/Fil:Batman_(black_background).jpg"},
                {"Aragorn","Elessar","Male","https://commons.wikimedia.org/wiki/File:Aragorn.png"},
                {"Victor Fries", "Mr Freeze", "Male","https://en.wikipedia.org/wiki/Mr._Freeze#/media/File:Mr._Freeze_Batman_Annual_Vol_2_1.png" }
                });

            migrationBuilder.InsertData(table: "Actors",
              columns: new[] { "FirstName", "LastName", "Gender", "DateOfBirth", "PlaceOfBirth", "Biography", "PictureUrl" },
                values: new object[,] { { "George", "Clooney", "Male", new DateTime(1961, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Kentucky", "George Timothy Clooney (born May 6, 1961) is an American actor, film director, producer, and screenwriter. ", "https://en.wikipedia.org/wiki/George_Clooney#/media/File:George_Clooney_2016.jpg" },
                { "Viggo", "Mortensen", "Male", new DateTime(1961, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Manhattan", "Viggo Peter Mortensen jr. (født 20. oktober 1958 i New York City) er en dansk-amerikansk Oscar- og Golden Globe-nominert skuespiller, poet, fotograf og maler, mest kjent for rollen som «Aragorn» i Ringenes herre-filmtrilogien.", "https://no.wikipedia.org/wiki/Viggo_Mortensen#/media/Fil:Viggo_Mortensen_Cannes_2016.jpg" },
                { "Christian", "Bale", "Male", new DateTime(1961, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "HaverFordWest", "Christian Charles Philip Bale (født 30. januar 1974) er en britisk skuespiller. Han er kjent for sine roller i Terminator Salvation, American Psycho, Solens rike, Batman Begins, The Dark Knight, The Dark Knight Rises, The Fighter og American Hustle.", "https://no.wikipedia.org/wiki/Christian_Bale#/media/Fil:Christian_Bale-7837.jpg" },
                { "Arnold", "Scwarchneger", "Male", new DateTime(1961, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Austria", "Arnold Alois Schwarzenegger  is an Austrian-American actor, businessman, former politician and professional bodybuilder.", "https://en.wikipedia.org/wiki/Arnold_Schwarzenegger#/media/File:Arnold_Schwarzenegger_by_Gage_Skidmore_4.jpg" }
                });

            migrationBuilder.InsertData(table: "MovieCharacter",
                columns: new[] { "ActorId", "CharacterId", "MovieId" },
                values: new object[,] {
                    {1,1,2},
                        {2,2,1},
                        {3,1,3},
                        {4,3,2}
                });

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}

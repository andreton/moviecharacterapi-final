﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharacterAPI_Final.DTOs.ActorsDTO
{
    public class ActorsListDto
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
    }
}
